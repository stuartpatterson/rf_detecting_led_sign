EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L callsign_led_onair_sign-rescue:Jack-DC-Connector J1
U 1 1 610D92FD
P 1250 1250
F 0 "J1" H 1307 1575 50  0000 C CNN
F 1 "Jack-DC" H 1307 1484 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 1300 1210 50  0001 C CNN
F 3 "~" H 1300 1210 50  0001 C CNN
	1    1250 1250
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:SW_SPST-Switch SW1
U 1 1 610D9C60
P 1900 1150
F 0 "SW1" H 1900 1385 50  0000 C CNN
F 1 "SW_SPST" H 1900 1294 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1900 1150 50  0001 C CNN
F 3 "~" H 1900 1150 50  0001 C CNN
	1    1900 1150
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR01
U 1 1 610EE367
P 1700 3000
F 0 "#PWR01" H 1700 2750 50  0001 C CNN
F 1 "GND" H 1705 2827 50  0000 C CNN
F 2 "" H 1700 3000 50  0001 C CNN
F 3 "" H 1700 3000 50  0001 C CNN
	1    1700 3000
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:C-Device C1
U 1 1 610EF9D2
P 2500 2700
F 0 "C1" V 2752 2700 50  0000 C CNN
F 1 "9.1pF" V 2661 2700 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 2538 2550 50  0001 C CNN
F 3 "~" H 2500 2700 50  0001 C CNN
	1    2500 2700
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:C-Device C2
U 1 1 610F29AE
P 3200 2550
F 0 "C2" H 3315 2596 50  0000 L CNN
F 1 "1.5nF" H 3315 2505 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3238 2400 50  0001 C CNN
F 3 "~" H 3200 2550 50  0001 C CNN
	1    3200 2550
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:L-Device L2
U 1 1 610F3113
P 3500 2550
F 0 "L2" H 3553 2596 50  0000 L CNN
F 1 "1uH" H 3553 2505 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L7.0mm_D3.3mm_P12.70mm_Horizontal_Fastron_MICC" H 3500 2550 50  0001 C CNN
F 3 "~" H 3500 2550 50  0001 C CNN
	1    3500 2550
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:C-Device C3
U 1 1 610F3C04
P 4050 2550
F 0 "C3" V 3798 2550 50  0000 C CNN
F 1 "0.01uF" V 3889 2550 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4088 2400 50  0001 C CNN
F 3 "~" H 4050 2550 50  0001 C CNN
	1    4050 2550
	0    1    1    0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR03
U 1 1 610F6DB7
P 2650 2850
F 0 "#PWR03" H 2650 2600 50  0001 C CNN
F 1 "GND" H 2655 2677 50  0000 C CNN
F 2 "" H 2650 2850 50  0001 C CNN
F 3 "" H 2650 2850 50  0001 C CNN
	1    2650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1150 1700 1150
$Comp
L callsign_led_onair_sign-rescue:D-Device D9
U 1 1 6111A5E1
P 2250 1150
F 0 "D9" H 2250 933 50  0000 C CNN
F 1 "1N4001" H 2250 1024 50  0000 C CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2250 1150 50  0001 C CNN
F 3 "~" H 2250 1150 50  0001 C CNN
	1    2250 1150
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR02
U 1 1 6111B298
P 1550 1650
F 0 "#PWR02" H 1550 1400 50  0001 C CNN
F 1 "GND" H 1555 1477 50  0000 C CNN
F 2 "" H 1550 1650 50  0001 C CNN
F 3 "" H 1550 1650 50  0001 C CNN
	1    1550 1650
	1    0    0    -1  
$EndComp
Text Label 2400 1150 0    50   ~ 0
+5V
$Comp
L callsign_led_onair_sign-rescue:R_POT-Device SENSITIVITY_RV1
U 1 1 61131FBE
P 7350 3300
F 0 "SENSITIVITY_RV1" V 7235 3300 50  0000 C CNN
F 1 "10K" V 7144 3300 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09L_Single_Vertical" H 7350 3300 50  0001 C CNN
F 3 "~" H 7350 3300 50  0001 C CNN
	1    7350 3300
	0    -1   -1   0   
$EndComp
Text Label 7100 3250 1    50   ~ 0
+5V
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR07
U 1 1 61133446
P 7500 3300
F 0 "#PWR07" H 7500 3050 50  0001 C CNN
F 1 "GND" H 7505 3127 50  0000 C CNN
F 2 "" H 7500 3300 50  0001 C CNN
F 3 "" H 7500 3300 50  0001 C CNN
	1    7500 3300
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:IRLB8721PBF-Transistor_FET Q2
U 1 1 6113676F
P 9800 1900
F 0 "Q2" H 10004 1946 50  0000 L CNN
F 1 "IRLB8721PBF" H 10004 1855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 10050 1825 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlb8721pbf.pdf?fileId=5546d462533600a40153566056732591" H 9800 1900 50  0001 L CNN
	1    9800 1900
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:IRLB8721PBF-Transistor_FET Q3
U 1 1 61137628
P 10150 3700
F 0 "Q3" H 10354 3746 50  0000 L CNN
F 1 "IRLB8721PBF" H 10354 3655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 10400 3625 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irlb8721pbf.pdf?fileId=5546d462533600a40153566056732591" H 10150 3700 50  0001 L CNN
	1    10150 3700
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR09
U 1 1 61139A18
P 9900 2350
F 0 "#PWR09" H 9900 2100 50  0001 C CNN
F 1 "GND" H 9905 2177 50  0000 C CNN
F 2 "" H 9900 2350 50  0001 C CNN
F 3 "" H 9900 2350 50  0001 C CNN
	1    9900 2350
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D2
U 1 1 6113C216
P 2900 4700
F 0 "D2" V 2939 4582 50  0000 R CNN
F 1 "LED_RED" V 2800 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 2900 4700 50  0001 C CNN
F 3 "~" H 2900 4700 50  0001 C CNN
	1    2900 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R2
U 1 1 6113D515
P 2900 4400
F 0 "R2" V 2693 4400 50  0000 C CNN
F 1 "220" V 2784 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2830 4400 50  0001 C CNN
F 3 "~" H 2900 4400 50  0001 C CNN
	1    2900 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D1
U 1 1 61143031
P 2850 6500
F 0 "D1" V 2889 6382 50  0000 R CNN
F 1 "LED_WHITE" V 2750 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 2850 6500 50  0001 C CNN
F 3 "~" H 2850 6500 50  0001 C CNN
	1    2850 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R1
U 1 1 61143037
P 2850 6200
F 0 "R1" V 2643 6200 50  0000 C CNN
F 1 "220" V 2734 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2780 6200 50  0001 C CNN
F 3 "~" H 2850 6200 50  0001 C CNN
	1    2850 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D4
U 1 1 61143AE1
P 3200 6500
F 0 "D4" V 3239 6382 50  0000 R CNN
F 1 "LED_WHITE" V 2950 6700 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3200 6500 50  0001 C CNN
F 3 "~" H 3200 6500 50  0001 C CNN
	1    3200 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R3
U 1 1 61143AE7
P 3200 6200
F 0 "R3" V 2993 6200 50  0000 C CNN
F 1 "220" V 3084 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3130 6200 50  0001 C CNN
F 3 "~" H 3200 6200 50  0001 C CNN
	1    3200 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D7
U 1 1 61144711
P 3550 6500
F 0 "D7" V 3589 6382 50  0000 R CNN
F 1 "LED_WHITE" V 3450 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3550 6500 50  0001 C CNN
F 3 "~" H 3550 6500 50  0001 C CNN
	1    3550 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R5
U 1 1 61144717
P 3550 6200
F 0 "R5" V 3343 6200 50  0000 C CNN
F 1 "220" V 3434 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3480 6200 50  0001 C CNN
F 3 "~" H 3550 6200 50  0001 C CNN
	1    3550 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D10
U 1 1 61145257
P 3900 6500
F 0 "D10" V 3939 6382 50  0000 R CNN
F 1 "LED_WHITE" V 3650 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3900 6500 50  0001 C CNN
F 3 "~" H 3900 6500 50  0001 C CNN
	1    3900 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R7
U 1 1 6114525D
P 3900 6200
F 0 "R7" V 3693 6200 50  0000 C CNN
F 1 "220" V 3784 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3830 6200 50  0001 C CNN
F 3 "~" H 3900 6200 50  0001 C CNN
	1    3900 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D12
U 1 1 61145E03
P 4250 6500
F 0 "D12" V 4289 6382 50  0000 R CNN
F 1 "LED_WHITE" V 4150 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4250 6500 50  0001 C CNN
F 3 "~" H 4250 6500 50  0001 C CNN
	1    4250 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R9
U 1 1 61145E09
P 4250 6200
F 0 "R9" V 4043 6200 50  0000 C CNN
F 1 "220" V 4134 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4180 6200 50  0001 C CNN
F 3 "~" H 4250 6200 50  0001 C CNN
	1    4250 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D5
U 1 1 61146B91
P 3250 4700
F 0 "D5" V 3289 4582 50  0000 R CNN
F 1 "LED_RED" V 3150 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3250 4700 50  0001 C CNN
F 3 "~" H 3250 4700 50  0001 C CNN
	1    3250 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R4
U 1 1 61146B97
P 3250 4400
F 0 "R4" V 3043 4400 50  0000 C CNN
F 1 "220" V 3134 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 4400 50  0001 C CNN
F 3 "~" H 3250 4400 50  0001 C CNN
	1    3250 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D8
U 1 1 611478BF
P 3600 4700
F 0 "D8" V 3639 4582 50  0000 R CNN
F 1 "LED_RED" V 3500 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3600 4700 50  0001 C CNN
F 3 "~" H 3600 4700 50  0001 C CNN
	1    3600 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R6
U 1 1 611478C5
P 3600 4400
F 0 "R6" V 3393 4400 50  0000 C CNN
F 1 "220" V 3484 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 4400 50  0001 C CNN
F 3 "~" H 3600 4400 50  0001 C CNN
	1    3600 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D11
U 1 1 6114835B
P 3950 4700
F 0 "D11" V 3989 4582 50  0000 R CNN
F 1 "LED_RED" V 3850 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3950 4700 50  0001 C CNN
F 3 "~" H 3950 4700 50  0001 C CNN
	1    3950 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R8
U 1 1 61148361
P 3950 4400
F 0 "R8" V 3743 4400 50  0000 C CNN
F 1 "220" V 3834 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3880 4400 50  0001 C CNN
F 3 "~" H 3950 4400 50  0001 C CNN
	1    3950 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D13
U 1 1 61148E71
P 4300 4700
F 0 "D13" V 4339 4582 50  0000 R CNN
F 1 "LED_RED" V 4200 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4300 4700 50  0001 C CNN
F 3 "~" H 4300 4700 50  0001 C CNN
	1    4300 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R10
U 1 1 61148E77
P 4300 4400
F 0 "R10" V 4093 4400 50  0000 C CNN
F 1 "220" V 4184 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 4400 50  0001 C CNN
F 3 "~" H 4300 4400 50  0001 C CNN
	1    4300 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	2900 4850 2900 5050
Wire Wire Line
	2900 5050 3250 5050
Wire Wire Line
	4300 4850 4300 5050
Wire Wire Line
	4300 5050 3950 5050
Wire Wire Line
	3950 4850 3950 5050
Connection ~ 3950 5050
Wire Wire Line
	3950 5050 3600 5050
Wire Wire Line
	3600 4850 3600 5050
Connection ~ 3600 5050
Wire Wire Line
	3250 4850 3250 5050
Connection ~ 3250 5050
Wire Wire Line
	2850 6650 2850 6900
Wire Wire Line
	2850 6900 3200 6900
Wire Wire Line
	3550 6900 3900 6900
Wire Wire Line
	4250 6900 4250 6650
Connection ~ 3550 6900
Wire Wire Line
	3900 6650 3900 6900
Connection ~ 3900 6900
Wire Wire Line
	3900 6900 4250 6900
Wire Wire Line
	3550 6650 3550 6900
Wire Wire Line
	3200 6650 3200 6900
Connection ~ 3200 6900
Wire Wire Line
	3200 6900 3550 6900
Wire Wire Line
	2850 6050 3200 6050
Wire Wire Line
	4250 6050 3900 6050
Connection ~ 3900 6050
Wire Wire Line
	3550 6050 3200 6050
Connection ~ 3550 6050
Connection ~ 3200 6050
Wire Wire Line
	2900 4250 3250 4250
Wire Wire Line
	3250 4250 3600 4250
Connection ~ 3250 4250
Connection ~ 3600 4250
Wire Wire Line
	3950 4250 4300 4250
Connection ~ 3950 4250
Wire Wire Line
	3250 5050 3600 5050
Wire Wire Line
	9900 1700 10150 1700
Wire Wire Line
	2700 5050 2900 5050
Connection ~ 2900 5050
Wire Wire Line
	2700 6900 2850 6900
Connection ~ 2850 6900
$Comp
L callsign_led_onair_sign-rescue:R-Device R25
U 1 1 611D418B
P 9550 2150
F 0 "R25" H 9480 2104 50  0000 R CNN
F 1 "10K" H 9480 2195 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9480 2150 50  0001 C CNN
F 3 "~" H 9550 2150 50  0001 C CNN
	1    9550 2150
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R26
U 1 1 611D4DBD
P 9950 4000
F 0 "R26" H 9880 3954 50  0000 R CNN
F 1 "10K" H 9880 4045 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9880 4000 50  0001 C CNN
F 3 "~" H 9950 4000 50  0001 C CNN
	1    9950 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	9550 1900 9550 2000
Wire Wire Line
	9550 1900 9600 1900
Wire Wire Line
	9550 2300 9900 2300
Wire Wire Line
	9900 2300 9900 2350
Wire Wire Line
	9900 2100 9900 2300
Connection ~ 9900 2300
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR04
U 1 1 61209589
P 4250 3050
F 0 "#PWR04" H 4250 2800 50  0001 C CNN
F 1 "GND" H 4255 2877 50  0000 C CNN
F 2 "" H 4250 3050 50  0001 C CNN
F 3 "" H 4250 3050 50  0001 C CNN
	1    4250 3050
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:D_Schottky-Device D22
U 1 1 61212022
P 4250 2800
F 0 "D22" V 4204 2880 50  0000 L CNN
F 1 "1N5711" V 4295 2880 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 4250 2800 50  0001 C CNN
F 3 "~" H 4250 2800 50  0001 C CNN
	1    4250 2800
	0    1    1    0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:D_Schottky-Device D3
U 1 1 612123A5
P 1600 2700
F 0 "D3" V 1550 2500 50  0000 L CNN
F 1 "1N5711" H 1600 2600 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1600 2700 50  0001 C CNN
F 3 "~" H 1600 2700 50  0001 C CNN
	1    1600 2700
	0    1    1    0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:D_Schottky-Device D6
U 1 1 61212E6F
P 1850 2700
F 0 "D6" V 1896 2620 50  0000 R CNN
F 1 "1N5711" H 1850 2600 50  0000 R CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 1850 2700 50  0001 C CNN
F 3 "~" H 1850 2700 50  0001 C CNN
	1    1850 2700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR08
U 1 1 61227F83
P 8850 4300
F 0 "#PWR08" H 8850 4050 50  0001 C CNN
F 1 "GND" H 8855 4127 50  0000 C CNN
F 2 "" H 8850 4300 50  0001 C CNN
F 3 "" H 8850 4300 50  0001 C CNN
	1    8850 4300
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R24
U 1 1 6122A26E
P 8850 3650
F 0 "R24" H 8780 3604 50  0000 R CNN
F 1 "10K" H 8780 3695 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8780 3650 50  0001 C CNN
F 3 "~" H 8850 3650 50  0001 C CNN
	1    8850 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	8850 3800 8850 3850
Text Label 8850 3500 1    50   ~ 0
+5V
Wire Wire Line
	8550 4100 8450 4100
Wire Wire Line
	7100 3250 7100 3300
Wire Wire Line
	7100 3300 7200 3300
Wire Wire Line
	9950 4150 10100 4150
Wire Wire Line
	10250 4150 10250 3900
$Comp
L callsign_led_onair_sign-rescue:LED-Device D15
U 1 1 61255A04
P 4700 4700
F 0 "D15" V 4739 4582 50  0000 R CNN
F 1 "LED_RED" V 4600 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4700 4700 50  0001 C CNN
F 3 "~" H 4700 4700 50  0001 C CNN
	1    4700 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R12
U 1 1 61255A0A
P 4700 4400
F 0 "R12" V 4493 4400 50  0000 C CNN
F 1 "220" V 4584 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4630 4400 50  0001 C CNN
F 3 "~" H 4700 4400 50  0001 C CNN
	1    4700 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D17
U 1 1 61255A10
P 5050 4700
F 0 "D17" V 5089 4582 50  0000 R CNN
F 1 "LED_RED" V 4950 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5050 4700 50  0001 C CNN
F 3 "~" H 5050 4700 50  0001 C CNN
	1    5050 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R14
U 1 1 61255A16
P 5050 4400
F 0 "R14" V 4843 4400 50  0000 C CNN
F 1 "220" V 4934 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4980 4400 50  0001 C CNN
F 3 "~" H 5050 4400 50  0001 C CNN
	1    5050 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D19
U 1 1 61255A1C
P 5400 4700
F 0 "D19" V 5439 4582 50  0000 R CNN
F 1 "LED_RED" V 5300 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5400 4700 50  0001 C CNN
F 3 "~" H 5400 4700 50  0001 C CNN
	1    5400 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R16
U 1 1 61255A22
P 5400 4400
F 0 "R16" V 5193 4400 50  0000 C CNN
F 1 "220" V 5284 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5330 4400 50  0001 C CNN
F 3 "~" H 5400 4400 50  0001 C CNN
	1    5400 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D21
U 1 1 61255A28
P 5750 4700
F 0 "D21" V 5789 4582 50  0000 R CNN
F 1 "LED_RED" V 5650 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5750 4700 50  0001 C CNN
F 3 "~" H 5750 4700 50  0001 C CNN
	1    5750 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R18
U 1 1 61255A2E
P 5750 4400
F 0 "R18" V 5543 4400 50  0000 C CNN
F 1 "220" V 5634 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5680 4400 50  0001 C CNN
F 3 "~" H 5750 4400 50  0001 C CNN
	1    5750 4400
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D24
U 1 1 61255A34
P 6100 4700
F 0 "D24" V 6139 4582 50  0000 R CNN
F 1 "LED_RED" V 6000 4850 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 6100 4700 50  0001 C CNN
F 3 "~" H 6100 4700 50  0001 C CNN
	1    6100 4700
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R20
U 1 1 61255A3A
P 6100 4400
F 0 "R20" V 5893 4400 50  0000 C CNN
F 1 "220" V 5984 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6030 4400 50  0001 C CNN
F 3 "~" H 6100 4400 50  0001 C CNN
	1    6100 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4850 4700 5050
Wire Wire Line
	4700 5050 5050 5050
Wire Wire Line
	6100 4850 6100 5050
Wire Wire Line
	6100 5050 5750 5050
Wire Wire Line
	5750 4850 5750 5050
Connection ~ 5750 5050
Wire Wire Line
	5750 5050 5400 5050
Wire Wire Line
	5400 4850 5400 5050
Connection ~ 5400 5050
Wire Wire Line
	5050 4850 5050 5050
Connection ~ 5050 5050
Wire Wire Line
	4700 4250 5050 4250
Wire Wire Line
	5050 4250 5400 4250
Connection ~ 5050 4250
Connection ~ 5400 4250
Wire Wire Line
	5750 4250 6100 4250
Connection ~ 5750 4250
Wire Wire Line
	5050 5050 5400 5050
Connection ~ 4700 5050
$Comp
L callsign_led_onair_sign-rescue:LED-Device D14
U 1 1 612641BE
P 4600 6500
F 0 "D14" V 4639 6382 50  0000 R CNN
F 1 "LED_WHITE" V 4350 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4600 6500 50  0001 C CNN
F 3 "~" H 4600 6500 50  0001 C CNN
	1    4600 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R11
U 1 1 612641C4
P 4600 6200
F 0 "R11" V 4393 6200 50  0000 C CNN
F 1 "220" V 4484 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4530 6200 50  0001 C CNN
F 3 "~" H 4600 6200 50  0001 C CNN
	1    4600 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D16
U 1 1 612641CA
P 4950 6500
F 0 "D16" V 4989 6382 50  0000 R CNN
F 1 "LED_WHITE" V 4850 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4950 6500 50  0001 C CNN
F 3 "~" H 4950 6500 50  0001 C CNN
	1    4950 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R13
U 1 1 612641D0
P 4950 6200
F 0 "R13" V 4743 6200 50  0000 C CNN
F 1 "220" V 4834 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4880 6200 50  0001 C CNN
F 3 "~" H 4950 6200 50  0001 C CNN
	1    4950 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D18
U 1 1 612641D6
P 5300 6500
F 0 "D18" V 5339 6382 50  0000 R CNN
F 1 "LED_WHITE" V 5050 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5300 6500 50  0001 C CNN
F 3 "~" H 5300 6500 50  0001 C CNN
	1    5300 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R15
U 1 1 612641DC
P 5300 6200
F 0 "R15" V 5093 6200 50  0000 C CNN
F 1 "220" V 5184 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5230 6200 50  0001 C CNN
F 3 "~" H 5300 6200 50  0001 C CNN
	1    5300 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D20
U 1 1 612641E2
P 5650 6500
F 0 "D20" V 5689 6382 50  0000 R CNN
F 1 "LED_WHITE" V 5550 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5650 6500 50  0001 C CNN
F 3 "~" H 5650 6500 50  0001 C CNN
	1    5650 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R17
U 1 1 612641E8
P 5650 6200
F 0 "R17" V 5443 6200 50  0000 C CNN
F 1 "220" V 5534 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5580 6200 50  0001 C CNN
F 3 "~" H 5650 6200 50  0001 C CNN
	1    5650 6200
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LED-Device D23
U 1 1 612641EE
P 6000 6500
F 0 "D23" V 6039 6382 50  0000 R CNN
F 1 "LED_WHITE" V 5750 6650 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 6000 6500 50  0001 C CNN
F 3 "~" H 6000 6500 50  0001 C CNN
	1    6000 6500
	0    -1   -1   0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R19
U 1 1 612641F4
P 6000 6200
F 0 "R19" V 5793 6200 50  0000 C CNN
F 1 "220" V 5884 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5930 6200 50  0001 C CNN
F 3 "~" H 6000 6200 50  0001 C CNN
	1    6000 6200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 6650 4600 6900
Wire Wire Line
	4600 6900 4950 6900
Wire Wire Line
	5300 6900 5650 6900
Wire Wire Line
	6000 6900 6000 6650
Connection ~ 5300 6900
Wire Wire Line
	5650 6650 5650 6900
Connection ~ 5650 6900
Wire Wire Line
	5650 6900 6000 6900
Wire Wire Line
	5300 6650 5300 6900
Wire Wire Line
	4950 6650 4950 6900
Connection ~ 4950 6900
Wire Wire Line
	4950 6900 5300 6900
Wire Wire Line
	4600 6050 4950 6050
Wire Wire Line
	6000 6050 5650 6050
Connection ~ 5650 6050
Wire Wire Line
	5300 6050 4950 6050
Connection ~ 5300 6050
Connection ~ 4950 6050
Text Label 10550 1700 0    50   ~ 0
on_air_leds
Wire Wire Line
	10250 3500 10250 3450
Wire Wire Line
	10250 3450 10350 3450
Text Label 10400 3450 0    50   ~ 0
off_air_leds
Wire Wire Line
	4300 5050 4700 5050
Connection ~ 4300 5050
Wire Wire Line
	4300 4250 4450 4250
Connection ~ 4300 4250
Connection ~ 4700 4250
Wire Wire Line
	5300 6050 5650 6050
Wire Wire Line
	4250 6900 4600 6900
Connection ~ 4250 6900
Connection ~ 4600 6900
Wire Wire Line
	4250 6050 4400 6050
Connection ~ 4250 6050
Connection ~ 4600 6050
Wire Wire Line
	5400 4250 5750 4250
Wire Wire Line
	3600 4250 3950 4250
Wire Wire Line
	4450 4100 4450 4250
Connection ~ 4450 4250
Wire Wire Line
	4450 4250 4700 4250
Wire Wire Line
	3550 6050 3900 6050
Connection ~ 4400 6050
Wire Wire Line
	4400 6050 4600 6050
Wire Wire Line
	4250 2950 4250 3050
$Comp
L callsign_led_onair_sign-rescue:PWR_FLAG-power #FLG02
U 1 1 612D365E
P 2400 950
F 0 "#FLG02" H 2400 1025 50  0001 C CNN
F 1 "PWR_FLAG" H 2400 1123 50  0000 C CNN
F 2 "" H 2400 950 50  0001 C CNN
F 3 "~" H 2400 950 50  0001 C CNN
	1    2400 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 950  2400 1000
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR010
U 1 1 612D8B41
P 10100 4150
F 0 "#PWR010" H 10100 3900 50  0001 C CNN
F 1 "GND" H 10105 3977 50  0000 C CNN
F 2 "" H 10100 4150 50  0001 C CNN
F 3 "" H 10100 4150 50  0001 C CNN
	1    10100 4150
	1    0    0    -1  
$EndComp
Connection ~ 10100 4150
Wire Wire Line
	10100 4150 10250 4150
Wire Wire Line
	1550 1350 1550 1500
$Comp
L callsign_led_onair_sign-rescue:PWR_FLAG-power #FLG01
U 1 1 612DCFE5
P 1700 1500
F 0 "#FLG01" H 1700 1575 50  0001 C CNN
F 1 "PWR_FLAG" V 1700 1628 50  0000 L CNN
F 2 "" H 1700 1500 50  0001 C CNN
F 3 "~" H 1700 1500 50  0001 C CNN
	1    1700 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 1500 1700 1500
Connection ~ 1550 1500
Wire Wire Line
	1550 1500 1550 1650
$Comp
L callsign_led_onair_sign-rescue:L-Device L1
U 1 1 6131FDE8
P 2850 2700
F 0 "L1" H 2903 2746 50  0000 L CNN
F 1 "13uH" H 2903 2655 50  0000 L CNN
F 2 "Inductor_THT:L_Axial_L7.0mm_D3.3mm_P12.70mm_Horizontal_Fastron_MICC" H 2850 2700 50  0001 C CNN
F 3 "~" H 2850 2700 50  0001 C CNN
	1    2850 2700
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:D_Schottky-Device D25
U 1 1 611FFE1A
P 5150 2550
F 0 "D25" H 5100 2650 50  0000 L CNN
F 1 "1N5711" H 5050 2450 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 5150 2550 50  0001 C CNN
F 3 "~" H 5150 2550 50  0001 C CNN
	1    5150 2550
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:C-Device C4
U 1 1 61200FA7
P 5300 2800
F 0 "C4" V 5048 2800 50  0000 C CNN
F 1 "0.01uF" V 5139 2800 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5338 2650 50  0001 C CNN
F 3 "~" H 5300 2800 50  0001 C CNN
	1    5300 2800
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR05
U 1 1 612021B6
P 5300 3050
F 0 "#PWR05" H 5300 2800 50  0001 C CNN
F 1 "GND" H 5305 2877 50  0000 C CNN
F 2 "" H 5300 3050 50  0001 C CNN
F 3 "" H 5300 3050 50  0001 C CNN
	1    5300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2550 4250 2550
Wire Wire Line
	4250 2550 4250 2650
Wire Wire Line
	600  2550 650  2550
Wire Notes Line
	1000 2400 1000 3250
Wire Notes Line
	1000 3250 2100 3250
Wire Notes Line
	2100 3250 2100 2400
Wire Notes Line
	2100 2400 1000 2400
Wire Notes Line
	3850 3300 4700 3300
Wire Wire Line
	4250 2550 4800 2550
Connection ~ 4250 2550
Wire Wire Line
	5300 2650 5300 2550
Wire Wire Line
	5300 3050 5300 2950
Wire Notes Line
	4900 3300 5700 3300
Wire Notes Line
	5700 3300 5700 2150
Wire Notes Line
	5700 2150 4900 2150
Wire Notes Line
	4900 2150 4900 3300
Wire Wire Line
	7100 3000 7350 3000
Wire Wire Line
	7350 3000 7350 3150
Wire Notes Line
	8200 2150 8200 3600
Wire Notes Line
	8200 3600 6950 3600
Wire Notes Line
	6950 3600 6950 2150
Wire Notes Line
	6950 2150 8200 2150
Wire Notes Line
	9350 2650 11100 2650
Wire Notes Line
	11100 2650 11100 1500
Wire Notes Line
	11100 1500 9350 1500
Wire Notes Line
	9350 1500 9350 2650
Wire Wire Line
	9950 3700 9950 3850
Wire Notes Line
	8350 4650 9550 4650
Wire Notes Line
	9550 4650 9550 3150
Wire Notes Line
	9550 3150 8350 3150
Wire Notes Line
	8350 3150 8350 4650
Wire Notes Line
	9700 4600 11100 4600
Wire Notes Line
	11100 4600 11100 3250
Wire Notes Line
	11100 3250 9700 3250
Wire Notes Line
	9700 3250 9700 4600
Wire Wire Line
	9100 2000 9550 2000
Connection ~ 9550 2000
Wire Wire Line
	8850 3850 9200 3850
Connection ~ 8850 3850
Wire Wire Line
	8850 3850 8850 3900
Connection ~ 9950 3850
$Comp
L callsign_led_onair_sign-rescue:R-Device R23
U 1 1 61206AF4
P 8450 3750
F 0 "R23" H 8380 3704 50  0000 R CNN
F 1 "10K" H 8380 3795 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8380 3750 50  0001 C CNN
F 3 "~" H 8450 3750 50  0001 C CNN
	1    8450 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	8450 4100 8450 3900
$Comp
L callsign_led_onair_sign-rescue:LM358-Amplifier_Operational U1
U 1 1 61257C6A
P 6300 2550
F 0 "U1" H 6300 2917 50  0000 C CNN
F 1 "LM358" H 6300 2826 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6300 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6300 2550 50  0001 C CNN
	1    6300 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2550 5650 2550
Connection ~ 5300 2550
$Comp
L callsign_led_onair_sign-rescue:R-Device R21
U 1 1 6125DC85
P 6600 2700
F 0 "R21" H 6530 2654 50  0000 R CNN
F 1 "10K" H 6530 2745 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6530 2700 50  0001 C CNN
F 3 "~" H 6600 2700 50  0001 C CNN
	1    6600 2700
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:R-Device R22
U 1 1 6125E24B
P 6600 3000
F 0 "R22" H 6530 2954 50  0000 R CNN
F 1 "1K" H 6530 3045 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6530 3000 50  0001 C CNN
F 3 "~" H 6600 3000 50  0001 C CNN
	1    6600 3000
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:LM358-Amplifier_Operational U1
U 2 1 61273167
P 7450 2650
F 0 "U1" H 7450 3017 50  0000 C CNN
F 1 "LM358" H 7450 2926 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7450 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7450 2650 50  0001 C CNN
	2    7450 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2750 7150 2750
Wire Wire Line
	7100 2750 7100 3000
Wire Wire Line
	7750 2650 8350 2650
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR06
U 1 1 61292541
P 6600 3150
F 0 "#PWR06" H 6600 2900 50  0001 C CNN
F 1 "GND" H 6605 2977 50  0000 C CNN
F 2 "" H 6600 3150 50  0001 C CNN
F 3 "" H 6600 3150 50  0001 C CNN
	1    6600 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2450 5650 2550
Wire Wire Line
	6600 2850 6000 2850
Wire Wire Line
	6000 2850 6000 2650
Connection ~ 6600 2850
Connection ~ 6600 2550
Wire Wire Line
	6600 2550 7000 2550
Wire Wire Line
	8450 3600 8450 2650
Connection ~ 8450 2650
Wire Wire Line
	9100 2650 9100 2000
Wire Wire Line
	8450 2650 9100 2650
Wire Notes Line
	5850 2050 5850 3450
Wire Notes Line
	5850 3450 6900 3450
Wire Notes Line
	6900 3450 6900 2050
Wire Notes Line
	6900 2050 5850 2050
Text Notes 1050 2500 0    50   ~ 0
Power Protection
Text Notes 2450 2200 0    50   ~ 0
Band Pass Filter
Text Notes 3850 2200 0    50   ~ 0
Clamper DC Restorer
Wire Notes Line
	3850 2100 4700 2100
Wire Notes Line
	3850 2100 3850 3300
Wire Notes Line
	4700 2100 4700 3300
Text Notes 5100 2250 0    50   ~ 0
Peak Detector
Text Notes 6500 2150 0    50   ~ 0
Amplifier
Text Notes 7700 2250 0    50   ~ 0
Comparator
$Comp
L callsign_led_onair_sign-rescue:Antenna_Dipole-Device AE1
U 1 1 612F8807
P 600 2050
F 0 "AE1" H 830 1964 50  0000 L CNN
F 1 "Antenna_Dipole" H 830 1873 50  0000 L CNN
F 2 "Connector_Coaxial:BNC_Amphenol_B6252HB-NPP3G-50_Horizontal" H 600 2050 50  0001 C CNN
F 3 "~" H 600 2050 50  0001 C CNN
	1    600  2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  2250 600  2550
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR011
U 1 1 612FDC5D
P 700 2250
F 0 "#PWR011" H 700 2000 50  0001 C CNN
F 1 "GND" H 705 2077 50  0000 C CNN
F 2 "" H 700 2250 50  0001 C CNN
F 3 "" H 700 2250 50  0001 C CNN
	1    700  2250
	1    0    0    -1  
$EndComp
$Comp
L callsign_led_onair_sign-rescue:Conn_01x03_Male-Connector J2
U 1 1 613CDF54
P 3700 5350
F 0 "J2" H 3808 5631 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3808 5540 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 3700 5350 50  0001 C CNN
F 3 "~" H 3700 5350 50  0001 C CNN
	1    3700 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 5450 4400 5850
Text Label 1150 5250 0    50   ~ 0
on_air_leds
Text Label 1150 5150 0    50   ~ 0
off_air_leds
Text Label 1150 5050 0    50   ~ 0
+5V
$Comp
L callsign_led_onair_sign-rescue:Conn_01x03_Male-Connector J3
U 1 1 6142EEF1
P 950 5150
F 0 "J3" H 1058 5431 50  0000 C CNN
F 1 "Conn_01x03_Male" H 1058 5340 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 950 5150 50  0001 C CNN
F 3 "~" H 950 5150 50  0001 C CNN
	1    950  5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 5050 2700 5550
Wire Wire Line
	2700 5550 3600 5550
Wire Wire Line
	2700 6900 2700 5650
Wire Wire Line
	2700 5650 3700 5650
Wire Wire Line
	3700 5650 3700 5550
Wire Wire Line
	4450 4100 6400 4100
Wire Wire Line
	6400 4100 6400 5450
Wire Wire Line
	6400 5450 4400 5450
Wire Wire Line
	3800 5550 3800 5850
Wire Wire Line
	3800 5850 4400 5850
Connection ~ 4400 5850
Wire Wire Line
	4400 5850 4400 6050
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP1
U 1 1 61555D81
P 650 2550
F 0 "TP1" H 600 2650 50  0000 R CNN
F 1 "Antenna" H 800 2750 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 850 2550 50  0001 C CNN
F 3 "~" H 850 2550 50  0001 C CNN
	1    650  2550
	-1   0    0    1   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP2
U 1 1 61556CEA
P 2150 2250
F 0 "TP2" H 2100 2350 50  0000 R CNN
F 1 "Power Protection" H 2300 2450 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 2350 2250 50  0001 C CNN
F 3 "~" H 2350 2250 50  0001 C CNN
	1    2150 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2250 2150 2550
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP3
U 1 1 6155D867
P 2600 1000
F 0 "TP3" H 2550 1100 50  0000 R CNN
F 1 "+5 Volts" H 2750 1200 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 2800 1000 50  0001 C CNN
F 3 "~" H 2800 1000 50  0001 C CNN
	1    2600 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 1000 2400 1000
Connection ~ 2400 1000
Wire Wire Line
	2400 1000 2400 1150
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP4
U 1 1 615641A6
P 3800 2150
F 0 "TP4" H 3750 2250 50  0000 R CNN
F 1 "Band Pass" H 3950 2350 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 4000 2150 50  0001 C CNN
F 3 "~" H 4000 2150 50  0001 C CNN
	1    3800 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2150 3800 2550
Wire Wire Line
	3800 2550 3900 2550
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP5
U 1 1 6156AFC3
P 4800 2150
F 0 "TP5" H 4750 2250 50  0000 R CNN
F 1 "Clamper" H 4950 2350 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 5000 2150 50  0001 C CNN
F 3 "~" H 5000 2150 50  0001 C CNN
	1    4800 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2150 4800 2550
Connection ~ 4800 2550
Wire Wire Line
	4800 2550 5000 2550
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP6
U 1 1 6157194C
P 5750 2050
F 0 "TP6" H 5700 2150 50  0000 R CNN
F 1 "Peak Detector" H 5900 2250 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 5950 2050 50  0001 C CNN
F 3 "~" H 5950 2050 50  0001 C CNN
	1    5750 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2050 5750 2450
Wire Wire Line
	5650 2450 5750 2450
Connection ~ 5750 2450
Wire Wire Line
	5750 2450 6000 2450
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP7
U 1 1 615790CB
P 6950 1950
F 0 "TP7" H 6900 2050 50  0000 R CNN
F 1 "Amplifier" H 7100 2150 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 7150 1950 50  0001 C CNN
F 3 "~" H 7150 1950 50  0001 C CNN
	1    6950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1950 6950 2100
Wire Wire Line
	6950 2100 7000 2100
Wire Wire Line
	7000 2100 7000 2550
Connection ~ 7000 2550
Wire Wire Line
	7000 2550 7150 2550
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP8
U 1 1 615801F9
P 8350 2100
F 0 "TP8" H 8300 2200 50  0000 R CNN
F 1 "Comparator" H 8500 2300 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 8550 2100 50  0001 C CNN
F 3 "~" H 8550 2100 50  0001 C CNN
	1    8350 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2100 8350 2650
Connection ~ 8350 2650
Wire Wire Line
	8350 2650 8450 2650
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP10
U 1 1 61587847
P 10150 1350
F 0 "TP10" H 10100 1450 50  0000 R CNN
F 1 "On Air" H 10300 1550 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 10350 1350 50  0001 C CNN
F 3 "~" H 10350 1350 50  0001 C CNN
	1    10150 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 1350 10150 1700
Connection ~ 10150 1700
Wire Wire Line
	10150 1700 10550 1700
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP11
U 1 1 6158ECAC
P 10350 3150
F 0 "TP11" H 10300 3250 50  0000 R CNN
F 1 "Off Air" H 10500 3350 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 10550 3150 50  0001 C CNN
F 3 "~" H 10550 3150 50  0001 C CNN
	1    10350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3150 10350 3450
Connection ~ 10350 3450
Wire Wire Line
	10350 3450 10400 3450
$Comp
L callsign_led_onair_sign-rescue:TestPoint-Connector TP9
U 1 1 615966F1
P 9200 3450
F 0 "TP9" H 9150 3550 50  0000 R CNN
F 1 "Not Condition" H 9350 3650 50  0000 R CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 9400 3450 50  0001 C CNN
F 3 "~" H 9400 3450 50  0001 C CNN
	1    9200 3450
	1    0    0    -1  
$EndComp
Connection ~ 9200 3850
Wire Wire Line
	9200 3850 9950 3850
Wire Wire Line
	9200 3450 9200 3850
$Comp
L callsign_led_onair_sign-rescue:Q_NPN_EBC-Device Q1
U 1 1 613C626B
P 8750 4100
F 0 "Q1" H 8940 4146 50  0000 L CNN
F 1 "2N6517" H 8940 4055 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92" H 8950 4200 50  0001 C CNN
F 3 "~" H 8750 4100 50  0001 C CNN
	1    8750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2550 2500 2550
Wire Wire Line
	2500 2550 2850 2550
Connection ~ 2500 2550
Wire Wire Line
	2500 2850 2650 2850
Connection ~ 2650 2850
Wire Wire Line
	2650 2850 2850 2850
Wire Wire Line
	2850 2550 3050 2550
Connection ~ 2850 2550
Wire Wire Line
	3650 2550 3800 2550
Connection ~ 3800 2550
Wire Notes Line
	2300 2100 3750 2100
Wire Notes Line
	3750 2100 3750 3300
Wire Notes Line
	2300 2100 2300 3300
Wire Notes Line
	2300 3300 3750 3300
$Comp
L callsign_led_onair_sign-rescue:LM358-Amplifier_Operational U1
U 3 1 613F1EBE
P 6800 3900
F 0 "U1" V 6475 3900 50  0000 C CNN
F 1 "LM358" V 6566 3900 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6800 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6800 3900 50  0001 C CNN
	3    6800 3900
	0    1    1    0   
$EndComp
$Comp
L callsign_led_onair_sign-rescue:GND-power #PWR012
U 1 1 613F5E55
P 6500 3800
F 0 "#PWR012" H 6500 3550 50  0001 C CNN
F 1 "GND" H 6505 3627 50  0000 C CNN
F 2 "" H 6500 3800 50  0001 C CNN
F 3 "" H 6500 3800 50  0001 C CNN
	1    6500 3800
	0    1    1    0   
$EndComp
Text Label 7100 3800 0    50   ~ 0
+5V
$Comp
L Device:R R27
U 1 1 6156324A
P 1200 2550
F 0 "R27" V 1300 2550 50  0000 C CNN
F 1 "50" V 1400 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1130 2550 50  0001 C CNN
F 3 "~" H 1200 2550 50  0001 C CNN
	1    1200 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 2850 1700 2850
Wire Wire Line
	1700 3000 1700 2850
Connection ~ 1700 2850
Wire Wire Line
	1700 2850 1850 2850
Wire Wire Line
	1600 2550 1850 2550
Wire Wire Line
	1850 2550 2150 2550
Connection ~ 1850 2550
Connection ~ 2150 2550
Wire Wire Line
	650  2550 1050 2550
Connection ~ 650  2550
Wire Wire Line
	1350 2550 1600 2550
Connection ~ 1600 2550
$EndSCHEMATC
