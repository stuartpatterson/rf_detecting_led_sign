# RF Detecting LED Sign - On Air Amateur Radio Callsign Sign

[YouTube Video](https://youtu.be/X66dDvlhTHg)
Source code and any additional content provided for the mentioned project.

This KiCAD schematic and Inkscape SVG files should be used in addition to the YouTube video to create the
RF Detecting LED Sign.


This work is licensed under the Creative Commons 
Attribution-NonCommercial 4.0 International License. To view a copy
of this license, visit [http://creativecommons.org/licenses/by-nc/4.0/](http://creativecommons.org/licenses/by-nc/4.0/)

Any action you take upon the information in my YouTube videos or related schematics/stl/source code/additional content is strictly 
at your own risk and I will not be liable for losses, damages, or injuries in connection to the use of the videos or the recreation 
of the projects in the videos.  I am NOT a professional Electrical Engineer, nor am I licensed as an EE.